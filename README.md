# Implementace Conways Game of Life

Conway’s Game of Life je jednoduchý celulární automat, který je Turing kompletní.

## Minimální požadavky
Pochopte pravidla a naimplementujte simulátor Game of Life.

## Extra Points
Extra body jsou za následující rozšíření:

- [x] growable universe 
- [x] gliders 
- [x] glider guns 

(řešení obsahuje všechny extra body)

## Popis

### Soubor gol.rkt
Obsahuje logiku game of life. K dispozici jsou 3 funkce pro spuštění programu:
- animate
- animate-inf
- animate-size-inf

Další funkce jsou pomocné.

### Soubor unit-tests.rkt
Obsahuje základní testy pomocných funkcí z gol.rkt. 


### Soubor examples.rkt
Obsahuje příklady použití (všech 3 výše zmíněných funkcí z gol.rkt pro spuštění programu).
Příklady obsahují všechny patterny popsané na stránce - https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life.

#### Příklad spuštění se liší dle kategorie:

- Parametry:
	- iters = počet iterací
	- max-size = dvojice (cons width height) reprezentuje maximální velikost vykreslené desky. (při překročení max-size se deska vykreslí právě jednou a vykreslování skončí hláškou "end")
	- pattern-name = název patternu (všechny možnosti jsou uvedené v seznamu pod každou kategorii a jsou oddělené znakem '|' )


- "Still lifes": (pattern-name iters)
	- pattern-name = block | bee-hive | loaf | boat | tub
- "Oscillators": (pattern-name iters)
	- pattern-name = blinker | toad | beacon | pulsar | tub | penta-decathlon
- "Spaceships": 
	- (pattern-name iters)
		- pattern-name = glider	| diamond | glider-reverse | lwss | mwss | hwss
	- (pattern-name-inf max-size)
		- pattern-name-inf = glider-inf	| diamond-inf | glider-reverse-inf | lwss-inf | mwss-inf | hwss-inf
- "Glider guns": 
	- (pattern-name iters)
		- pattern-name = gosper-glider-gun | simkin-glider-gun | boat-bit-glider-gun
	- (pattern-name-inf max-size)
		- pattern-name-inf = gosper-glider-gun-inf | simkin-glider-gun-inf | boat-bit-glider-gun-inf


## Example usage

### "Still lifes"
```
(block 5)
```

### "Oscillators"
```
(penta-decathlon 16)
```

### "Spaceships"
```
(diamond 42)
(hwss-inf (cons 100 20))
```

### "Glider guns"
```
(gosper-glider-gun 200)
(boat-bit-glider-gun-inf (cons 100 200))
```