#lang racket
(require "gol.rkt")

; Unit Tests for [x y cell-sum inc-x inc-y dec in-list filter-not-in
;                 min-x min-y max-x max-y min-max to-pairs calc-min-x calc-min-y calc-max-x calc-max-y
;                 neighbours list-neighbour-cells alive-neighbours-cnt next-from-alives next-from-dead next]


; Helper function to compare 2 lists
(define (lists-equal? lst1 lst2)
  (__lists-equal-aux lst1 lst1 lst2 lst2))

(define (__lists-equal-aux lst1 lst1-tmp lst2 lst2-tmp)
  (if (null? lst1)
      (if (null? lst2)
          #t
          (if (in-list (car lst2) lst1-tmp)
              (__lists-equal-aux lst1 lst1-tmp (cdr lst2) lst2-tmp)
              #f))
       (if (in-list (car lst1) lst2-tmp)
              (__lists-equal-aux (cdr lst1) lst1-tmp lst2 lst2-tmp)
              #f)))
          
 ; x | y | cell-sum | inc-x | inc-y | dec
(define (test1)
  (equal? (x (cons 1 2)) 1))

(define (test2)
  (equal? (y (cons 1 2)) 2))

(define (test3)
  (equal? (cell-sum (cons 1 2) (cons 3 4)) (cons 4 6)))

(define (test4)
  (equal? (inc-x (cons 1 2)) (cons 2 2)))

(define (test5)
  (equal? (inc-y (cons 1 2)) (cons 1 3)))

(define (test6)
  (equal? (dec (cons 1 2)) (cons 0 1)))


; in-list 
(define (test7-1)
  (equal? (in-list 13 '()) false))


(define (test7-2)
  (equal? (in-list 13 '(1 2 3)) false))


(define (test7-3)
  (equal? (in-list 13 '(1 2 13)) true))


; filter-not-in
(define (test8-1)
  (lists-equal? (car (filter-not-in '(1 2 3 4 5) '(1 2 5 6 7))) '(3 4)))

(define (test8-2)
  (lists-equal? (cdr (filter-not-in '(1 2 3 4 5) '(1 2 5 6 7))) '(1 2 3 4 5 6 7)))


; min-x min-y | max-x max-y
(define (test9)
   (equal? 1 (min-x (cons (cons 1 2) (cons 3 4)))))

(define (test10)
   (equal? 2 (min-y (cons (cons 1 2) (cons 3 4)))))

(define (test11)
   (equal? 3 (max-x (cons (cons 1 2) (cons 3 4)))))

(define (test12)
   (equal? 4 (max-y (cons (cons 1 2) (cons 3 4)))))

; min-max
(define (test13)
   (equal? (min-max '((2 . 2) (1 . 2) (3 . 2)) (cons (cons 2 2) (cons 2 2))) (cons (cons 1 2) (cons 3 2))))


; to-pairs
(define (test14)
   (equal? (to-pairs 1 2 3 4) (cons (cons 1 2) (cons 3 4))))

; calc-min-x
(define (test15-1)
  (equal? (calc-min-x (cons 1 2) (cons (cons 2 5) (cons 7 7))) 1))

(define (test15-2)
  (equal? (calc-min-x (cons 2 2) (cons (cons 1 5) (cons 7 7))) 1))

; calc-min-y
(define (test16-1)
  (equal? (calc-min-y (cons 1 2) (cons (cons 2 5) (cons 7 7))) 2))

(define (test16-2)
  (equal? (calc-min-y (cons 2 5) (cons (cons 1 2) (cons 7 7))) 2))

; calc-max-x
(define (test17-1)
  (equal? (calc-max-x (cons 1 2) (cons (cons 2 5) (cons 7 6))) 7))

(define (test17-2)
  (equal? (calc-max-x (cons 7 6) (cons (cons 1 5) (cons 2 7))) 7))

; calc-max-y
(define (test18-1)
  (equal? (calc-max-y (cons 1 2) (cons (cons 2 5) (cons 6 7))) 7))

(define (test18-2)
  (equal? (calc-max-y (cons 2 7) (cons (cons 1 5) (cons 7 3))) 7))


; neighbours
(define (test19)
  (equal? (neighbours (cons 0 0)) '((1 . 1) (0 . 1) (-1 . 1) (1 . 0) (-1 . 0) (1 . -1) (0 . -1) (-1 . -1)))) 


; list-neighbour-coords
(define (test20)
  (lists-equal? (list-neighbour-cells '((0 . 0) (0 . 1))) '((0 . 0) (-1 . 2) (0 . 2) (1 . 2) (-1 . -1) (0 . -1) (1 . -1) (-1 . 0) (1 . 0) (-1 . 1) (0 . 1) (1 . 1))))


; alive-neighbour-cnt
(define (test21-1)
  (equal? (alive-neighbours-cnt (cons 0 0) '()) 0)) 

(define (test21-2)
  (equal? (alive-neighbours-cnt (cons 0 0) '((13 . 13) (7 . 7))) 0)) 

(define (test21-3)
  (equal? (alive-neighbours-cnt (cons 0 0) '((1 . 1))) 1)) 

(define (test21-4)
  (equal? (alive-neighbours-cnt (cons 0 0) '((13 . 13) (1 . 1) (7 . 7))) 1))

(define (test21-5)
  (equal? (alive-neighbours-cnt (cons 0 0) '((1 . 1) (-1 . -1))) 2))


; next-from-alive
(define (test22-1)
  (equal? (next-from-alives '((0 . 0))) '())) 

(define (test22-2)
  (equal? (next-from-alives '((-1 . 0) (0 . 0) (1 . 0))) '((0 . 0))))

(define (test22-3)
  (equal? (next-from-alives '((0 . -1) (0 . 0) (0 . 1))) '((0 . 0))))

(define (test22-4)
  (equal? (next-from-alives '((-1 . -1) (0 . 0) (1 . 1))) '((0 . 0))))

(define (test22-5)
   (lists-equal? (next-from-alives '((1 . 0) (0 . 0) (0 . 1))) '((1 . 0) (0 . 0) (0 . 1))) )


; next-from-dead
(define (test23-1)
   (lists-equal? (next-from-dead '((0 . -1) (0 . 0) (0 . 1)) '()) '((-1 . 0) (1 . 0))))

(define (test23-2)
   (lists-equal? (next-from-dead '((-1 . 0) (0 . 0) (1 . 0)) '()) '((0 . -1) (0 . 1))))

(define (test23-3)
   (lists-equal? (next-from-dead '((0 . 0) (2 . 0) (0 . 2)) '()) '((1 . 1))))


; next
(define (test24-1)
   (lists-equal? (next '((0 . -1) (0 . 0) (0 . 1))) '((-1 . 0) (0 . 0) (1 . 0))))

(define (test24-2)
   (lists-equal? (next '((-1 . 0) (0 . 0) (1 . 0))) '((0 . -1) (0 . 0) (0 . 1))))

(define (test24-3)
   (lists-equal? (next '((0 . 0) (2 . 0) (0 . 2))) '((1 . 1))))




(display "\n-------------- x | y | cell-sum | inc-x | inc-y | dec -------------------\n") 
(display "test-1 ") (test1)
(display "test-2 ") (test2)
(display "test-3 ") (test3)
(display "test-4 ") (test4)
(display "test-5 ") (test5)
(display "test-6 ") (test6)

(display "\n-------------- in-list --------------------------------------------------\n") 
(display "test-7-1 ") (test7-1)
(display "test-7-2 ") (test7-2)
(display "test-7-3 ") (test7-3)

(display "\n-------------- filter-not-in --------------------------------------------\n")
(display "test-8-1 ") (test8-1)
(display "test-8-2 ") (test8-2)

(display "\n-------------- min-x min-y | max-x max-y --------------------------------\n")
(display "test-9 ") (test9)
(display "test-10 ") (test10)
(display "test-11 ") (test11)
(display "test-12 ") (test12)


(display "\n-------------- min-max --------------------------------------------------\n")
(display "test-13 ") (test13)

(display "\n-------------- to-pairs -------------------------------------------------\n")
(display "test-14 ") (test14)

(display "\n-------------- calc-min-x | calc-min-y | calc-max-x | calc-max-y --------\n")
(display "test-15-1 ") (test15-1)
(display "test-15-2 ") (test15-2)

(display "test-16-1 ") (test16-1)
(display "test-16-2 ") (test16-2)

(display "test-17-1 ") (test17-1)
(display "test-17-2 ") (test17-2)

(display "test-18-1 ") (test18-1)
(display "test-18-2 ") (test18-2)


(display "\n-------------- neighbours -----------------------------------------------\n")
(display "test-19 ") (test19)

(display "\n-------------- list-neighbour-cells -------------------------------------\n")
(display "test-20 ") (test20)

(display "\n-------------- alive-neighbours-cnt -------------------------------------\n")
(display "test-21-1 ") (test21-1)
(display "test-21-2 ") (test21-2)
(display "test-21-3 ") (test21-3)
(display "test-21-4 ") (test21-4)
(display "test-21-5 ") (test21-5)


(display "\n-------------- next-from-alives -----------------------------------------\n")
(display "test-22-1 ") (test22-1)
(display "test-22-2 ") (test22-2)
(display "test-22-3 ") (test22-3)
(display "test-22-4 ") (test22-4)
(display "test-22-5 ") (test22-5)


(display "\n-------------- next-from-dead -------------------------------------------\n")
(display "test-23-1 ") (test23-1)
(display "test-23-2 ") (test23-2)
(display "test-23-3 ") (test23-3)


(display "\n-------------- next -----------------------------------------------------\n")
(display "test-24-1 ") (test24-1)
(display "test-24-2 ") (test24-2)
(display "test-24-3 ") (test24-3)

